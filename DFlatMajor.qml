import QtQuick 2.0
import "qrc:/staff"

Rectangle {
    FlatAccent{
        scale: .28
        x:-80; y:-45
    }

    FlatAccent{
        scale: .28
        x:-55; y:-75
    }

    FlatAccent{
        scale: .28
        x:-30; y:-35
    }

    FlatAccent{
        scale: .28
        x:-5; y:-65
    }

    FlatAccent{
        scale: .28
        x:20; y:-25
    }
}
