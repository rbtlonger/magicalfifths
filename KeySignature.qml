import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

import "qrc:/circleOfFifths"
import "qrc:/staff/keySignature"

ColumnLayout {

    property variant root: parent
    property double screenHeight: 800
    property double screenWidth: 600

    BackgroundCtrl{
        clock: circFifths.key
    }

    Rectangle{
        Layout.fillWidth: true
        Layout.maximumWidth: root.width
        Layout.fillHeight: false
        Layout.maximumHeight: 600
        Layout.minimumHeight: 200
        Layout.preferredHeight: 300
        color: "transparent"

        CircleOfFifths{
            id: circFifths
            anchors.verticalCenterOffset: 20
            scale: {
                var h = root.height * (6.5/10), w = root.width;
                console.log("::" + root.height + "; " + root.width)
                return (h > w ? w : h)/325;
            }
        }
    }

    Rectangle{
        Layout.fillWidth: true
        Layout.maximumWidth: root.width
        Layout.maximumHeight: 120
        Layout.minimumHeight: 50
        Layout.preferredHeight: 100
        color: "transparent"

        KeysText{
            anchors{
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
                verticalCenterOffset: 15
            }
            state: circFifths.key.toString()
            captionColor: "black"
            titleColor: "black"
            titleSize: 26
            captionSize: 22
        }
    }

    Rectangle{
        Layout.fillWidth: true
        Layout.maximumWidth: root.width
        Layout.maximumHeight: 250
        Layout.minimumHeight: 100
        Layout.preferredHeight: 150
        color: "transparent"

        KeySignatureControl {
            id: keyCtrl
            scale:{
                var os = Qt.platform.os;
                if(os == "blackberry"
                    || os == "android"
                    || os == "ios")
                    return 1.5;
                else return 1;
            }
            state: circFifths.key.toString();
            anchors{
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
        }
    }

}

