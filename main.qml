import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.0



ApplicationWindow {
    title: qsTr("Magical Fifths")
    width: 400
    height: 625

    color: "white"
    visible: true
    id:root

    ColumnLayout{
        anchors.fill: parent

        Rectangle {
            Layout.fillWidth: true
            Layout.maximumWidth: root.width
            Layout.maximumHeight: 30
            Layout.minimumHeight: 10
            Layout.preferredHeight: 20
            color: "transparent"

            ComboBox{
                id: cbxElms
                width: parent.width
                anchors.centerIn: parent

                style: ComboBoxStyle {
                    label: Label{
                        text: control.currentText
                        horizontalAlignment: Qt.AlignVCenter
                        font.capitalization: Font.SmallCaps
                    }
                }

                model: ListModel{
                    id: lstContent
                    ListElement{ text: "Key Signatures" }
                    ListElement{ text: "Elm 2" }
                    ListElement{ text: "Elm 3" }
                    ListElement{ text: "Elm 4" }
                }
            }

//            Text {
//                id: topTitle
//                text: "<b>Magical Fifths</b>"
//                width: root.width
//                horizontalAlignment: Text.AlignHCenter
//                color: "black"
//                font.pointSize: 18
//                font.letterSpacing: 8
//            }

        }

        Rectangle{
            Layout.fillWidth: true
            Layout.maximumWidth: root.width
            Layout.fillHeight: true
            Layout.maximumHeight: root.height
            color: "transparent"

            Loader{
                source: "/KeySignature.qml"
                width: parent.width
                height: parent.height

                anchors{
                    centerIn: parent
                }
            }
        }




    }



}
