import QtQuick 2.0

Rectangle {
    id: root
    state: "12"
    color: "transparent"
    height: 170
    width: 300

    KeySignatureStaff{
        id: staff; objectName: "staff"
        anchors.centerIn: parent
        z: 100
    }

    states:[
        //Sharps
        State{
            name: "12"
            onCompleted: createStaff("CMajor.qml")
        },
        State{
            name: "1"
            onCompleted: createStaff("GMajor.qml")
        },
        State{
            name: "2"
            onCompleted: createStaff("DMajor.qml")
        },
        State{
            name: "3"
            onCompleted: createStaff("AMajor.qml")
        },
        State{
            name: "4"
            onCompleted: createStaff("EMajor.qml")
        },
        State{
            name: "5sharp"
            onCompleted: createStaff("BMajor.qml")
        },
        State{
            name: "6sharp"
            onCompleted: createStaff("FSharpMajor.qml")
        },
        State{
            name: "7sharp"
            onCompleted: createStaff("CSharpMajor.qml")
        },

        //Flats
        State{
            name: "11"
            onCompleted: createStaff("FMajor.qml")
        },
        State{
            name: "10"
            onCompleted: createStaff("BFlatMajor.qml")
        },
        State{
            name: "9"
            onCompleted: createStaff("EFlatMajor.qml")
        },
        State{
            name: "8"
            onCompleted: createStaff("AFlatMajor.qml")
        },
        State{
            name: "7flat"
            onCompleted: createStaff("DFlatMajor.qml")
        },
        State{
            name: "6flat"
            onCompleted: createStaff("GFlatMajor.qml")
        },
        State{
            name: "5flat"
            onCompleted: createStaff("CFlatMajor.qml")
        }

    ]

    onStateChanged: clearStage();

    function clearStage(){
        for(var i=0; i<root.children.length; i++)
            if(root.children[i].objectName != "staff")
                root.children[i].destroy();
    }

    function createStaff(qmlCom){
        var com = Qt.createComponent(qmlCom);
        var obj = com.createObject(root);
        obj.anchors.centerIn = root;
        obj.z = 0;
    }

}

