import QtQuick 2.0
import QtGraphicalEffects 1.0

Item{
    anchors.centerIn: parent
    antialiasing: true
    rotation: 0
    state: "sharp"
    height: circ.height; width: circ.width

    property alias circ : circ;

    Rectangle{
        id: circ;
        color: "transparent"
        radius: width / 2
        height: 26; width: height
        antialiasing: true
        anchors.centerIn: parent

        RectangularGlow{
            id: circGlow
            glowRadius: 5
            spread: .011
            opacity: .25
            color: "yellow"
            anchors.fill: circ
            cornerRadius: circ.radius + glowRadius
        }

        Text{
            id: noteText
            color: "#333333"
            font.pointSize: 18
            text: "♯"
            anchors.centerIn: parent
        }
    }

    states: [
        State{
            name: "sharp"
            PropertyChanges {
                target: noteText
                text: "♯"
            }
        },
        State{
            name: "flat"
            PropertyChanges{
                target: noteText
                text: "♭"
            }
        }
    ]

}
