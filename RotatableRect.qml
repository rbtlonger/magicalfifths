import QtQuick 2.0

Rectangle{
    id: root;
    width: 720;
    height: 480;
    color: "black";

    Item {
        id: container;
        width: 250;
        height: width;
        anchors.centerIn: parent;

        property real centerX : (width / 2);
        property real centerY : (height / 2);

        Rectangle{
            id: rect;
            color: "white";
            transformOrigin: Item.Center;
            radius: (width / 2);
            antialiasing: true;
            anchors.fill: parent;

            Rectangle {
                id: handle;
                color: "red";
                width: 50;
                height: width;
                radius: (width / 2);
                antialiasing: true;
                anchors {
                    top: parent.top;
                    margins: 10;
                    horizontalCenter: parent.horizontalCenter;
                }

                MouseArea{
                    anchors.fill: parent;
                    onPositionChanged:  {
                        var point =  mapToItem (container, mouse.x, mouse.y);
                        var diffX = (point.x - container.centerX);
                        var diffY = (container.centerY - point.y);
                        var rad = Math.atan (diffY / diffX);
                        var deg = (rad * 180 / Math.PI);

                        if (diffX > 0 && diffY > 0) {
                            //quadrant 1
                            rect.rotation = 90 - Math.abs (deg);
                        } else if (diffX > 0 && diffY < 0) {
                            //quandrant 4
                            rect.rotation = 90 + Math.abs (deg);
                        } else if (diffX < 0 && diffY > 0) {
                            //quadrant 2
                            rect.rotation = 270 + Math.abs (deg);
                        } else if (diffX < 0 && diffY < 0) {
                            //quadrant 3
                            rect.rotation = 270 - Math.abs (deg);
                        }
                    }
                }
            }
        }

        Text {
            text: "%1 secs".arg (Math.round (rect.rotation / 6));
            font {
                pixelSize: 20;
                bold: true;
            }
            anchors.centerIn: parent;
        }
    }
}
