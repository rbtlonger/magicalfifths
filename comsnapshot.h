#ifndef COMSNAPSHOT_H
#define COMSNAPSHOT_H

#include <QObject>
#include <QQuickItem>
#include <QString>
#include <QQuickView>

class ComSnapshot : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double progress READ getProgress NOTIFY progressChanged)

public:
    explicit ComSnapshot(QQuickView* view, QObject *parent = 0);

    void take(QString const &path);
    double getProgress() const;

signals:
    void progressChanged() const;


public slots:
    void test();

private:
    QQuickView *view;
    double progress = 0.0;
};

#endif // COMSNAPSHOT_H
