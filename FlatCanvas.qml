import QtQuick 2.0

Canvas{
    canvasWindow: Qt.rect(0, 0, 2000, 2000)
    onImageLoaded : requestPaint();

    onPaint: {
        var ctx = getContext('2d');

      ctx.beginPath();
      ctx.moveTo(0,0);
      ctx.quadraticCurveTo(4,0,9,0);
      ctx.quadraticCurveTo(9,75,9,155);
      ctx.quadraticCurveTo(21,139,40,130);
      ctx.quadraticCurveTo(54,125,64,130);
      ctx.quadraticCurveTo(76,137,80,147);
      ctx.quadraticCurveTo(87,164,80,181);
      ctx.quadraticCurveTo(74,193,64,201);
      ctx.quadraticCurveTo(35,230,0,259);
      ctx.quadraticCurveTo(0,150,0,0);
      ctx.fill();

      ctx.fillStyle = "white";
      ctx.beginPath();
      ctx.moveTo(9,164);
      ctx.quadraticCurveTo(16,154,25,150);
      ctx.quadraticCurveTo(37,145,46,149);
      ctx.quadraticCurveTo(55,156,55,170);
      ctx.quadraticCurveTo(55,180,51,187);
      ctx.quadraticCurveTo(47,197,40,206);
      ctx.quadraticCurveTo(27,225,9,247);
      ctx.quadraticCurveTo(9,200,9,164);
      ctx.fill();
    }
}
