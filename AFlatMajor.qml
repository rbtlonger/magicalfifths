import QtQuick 2.0
import "qrc:/staff"

Rectangle {
    FlatAccent{
        scale: .28
        x:-80; y:-45
    }

    FlatAccent{
        scale: .28
        x:-60 + 5; y:-75
    }

    FlatAccent{
        scale: .28
        x:-40 + 10; y:-35
    }

    FlatAccent{
        scale: .28
        x:-5; y:-65
    }

}
