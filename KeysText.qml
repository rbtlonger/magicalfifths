import QtQuick 2.0

Rectangle{
    id: keysText

    property string captionColor: "black"
    property double captionSize: 16
    property double titleSize: 10
    property string titleColor: "black"

    Text{
        id: title
        horizontalAlignment: Text.AlignHCenter
        text: "C Major"
        color: titleColor
        font{
            pointSize: titleSize
        }
        anchors{
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text{
        id: caption
        horizontalAlignment: Text.AlignHCenter
        text: "C, G, D, A, B, F#, Db"
        color: captionColor
        font{
            pointSize: captionSize
        }
        anchors{
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
    }

    states: [
        State{
            name: "12"
            PropertyChanges {
                target: caption
                text: ""
            }
            PropertyChanges{
                target: title
                text: "C Major"
            }
        },
        State{
            name: "1"
            PropertyChanges {
                target: caption
                text: "F#"
            }
            PropertyChanges{
                target: title
                text: "G Major"
            }
        },
        State{
            name: "2"
            PropertyChanges {
                target: caption
                text: "F#, C#"
            }
            PropertyChanges{
                target: title
                text: "D Major"
            }
        },
        State{
            name: "3"
            PropertyChanges {
                target: caption
                text: "F#, C#, G#"
            }
            PropertyChanges{
                target: title
                text: "A Major"
            }
        },
        State{
            name: "4"
            PropertyChanges {
                target: caption
                text: "F#, C#, G#, D#"
            }
            PropertyChanges{
                target: title
                text: "E Major"
            }
        },
        State{
            name: "5sharp"
            PropertyChanges {
                target: caption
                text: "F#, C#, G#, D#, A#"
            }
            PropertyChanges{
                target: title
                text: "B Major"
            }
        },
        State{
            name: "6sharp"
            PropertyChanges {
                target: caption
                text: "F#, C#, G#, D#, A#, E#"
            }
            PropertyChanges{
                target: title
                text: "F# Major"
            }
        },
        State{
            name: "7sharp"
            PropertyChanges {
                target: caption
                text: "F#, C#, G#, D#, A#, E#, B#"
            }
            PropertyChanges{
                target: title
                text: "C# Major"
            }
        },

        State{
            name: "11"
            PropertyChanges {
                target: caption
                text: "Bb"
            }
            PropertyChanges{
                target: title
                text: "F Major"
            }
        },
        State{
            name: "10"
            PropertyChanges {
                target: caption
                text: "Bb, Eb"
            }
            PropertyChanges{
                target: title
                text: "Bb Major"
            }
        },
        State{
            name: "9"
            PropertyChanges {
                target: caption
                text: "Bb, Eb, Ab"
            }
            PropertyChanges{
                target: title
                text: "Eb Major"
            }
        },
        State{
            name: "8"
            PropertyChanges {
                target: caption
                text: "Bb, Eb, Ab, Db"
            }
            PropertyChanges{
                target: title
                text: "Ab Major"
            }
        },
        State{
            name: "7flat"
            PropertyChanges {
                target: caption
                text: "Bb, Eb, Ab, Db, Gb"
            }
            PropertyChanges{
                target: title
                text: "Db Major"
            }
        },
        State{
            name: "6flat"
            PropertyChanges {
                target: caption
                text: "Bb, Eb, Ab, Db, Gb, Cb"
            }
            PropertyChanges{
                target: title
                text: "Gb Major"
            }
        },
        State{
            name: "5flat"
            PropertyChanges {
                target: caption
                text: "Bb, Eb, Ab, Db, Gb, Cb, Fb"
            }
            PropertyChanges{
                target: title
                text: "Cb/B Major"
            }
        }
    ]

}
