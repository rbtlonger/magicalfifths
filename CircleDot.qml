import QtQuick 2.0
import QtGraphicalEffects 1.0

Item{
    transformOrigin: Item.Center
    anchors.fill: parent
    antialiasing: true
    rotation: 0

    property string noteText: "1";
    property int letterSpacing: 0;
    property string outerText: "1";
    property string insideText: "I";
    property alias circ : circ

    Rectangle{
        id: circ
        color: "white"
        radius: width / 2
        height: 40; width: height
        antialiasing: true
        anchors{
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            margins: 10
        }

        RectangularGlow{
            id: circGlow
            glowRadius: 5
            spread: .001
            opacity: 0
            color: "orange"
            anchors.fill: circ
            cornerRadius: circ.radius + glowRadius
        }

        Text{
            text: noteText
            font.pointSize: 14
            font.letterSpacing: letterSpacing;
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
        }

        OutsideText{
            id: txtOuter
            text: outerText
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.top
                verticalCenterOffset: -35
            }
        }

        OutsideText{
            id: txtInside
            text: insideText
            fontSize: 10
            fontColor: "#676767"
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.bottom
                verticalCenterOffset: 5
            }
        }
    }

    states: [
        State{
            name: "none"
            PropertyChanges {
                target: circ
                color: "white"
                border.width: 0
                border.color: "#000"
            }
            PropertyChanges{
                target: circGlow
                opacity:  0
            }
        },
        State {
            name: "compl"
            PropertyChanges {
                target: circ
                color: "#C98934"
                border.width: 1
                border.color: "black"
            }
        },
        State {
            name: "selected"
            PropertyChanges{
                target: circGlow
                opacity: .5
            }
            PropertyChanges {
                target: circ
                border.color: "black"
                border.width: 2
            }
        }
    ]

    transitions: [
        Transition {
            from: "none"
            to: "selected"
            NumberAnimation {
                target: circGlow
                property: "opacity"
                duration: 285
                easing.type: Easing.InBack
            }
        },
        Transition {
            from: "selected"
            to: "none"
            NumberAnimation {
                target: circGlow
                property: "opacity"
                duration: 800
                easing.type: Easing.OutBack
            }

            NumberAnimation {
                target: circ
                property: "color"
                duration: 400
                easing.type: Easing.InExpo
            }
        },

        Transition {
            from: "none"
            to: "compl"
            ColorAnimation{
                target: circ
                property: "color"
                duration: 800
                easing.type: Easing.InBack
            }
            NumberAnimation {
                target: circ
                property: "border.width"
                duration: 800
                easing.type: Easing.InBack
            }
        },
        Transition {
            from: "compl"
            to: "none"
            ColorAnimation {
                target: circ
                property: "color"
                duration: 800
                easing.type: Easing.OutBack
            }
            NumberAnimation {
                target: circ
                property: "border.width"
                duration: 800
                easing.type: Easing.OutBack
            }
        }
    ]
}
