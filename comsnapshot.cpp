#include "comsnapshot.h"

#include <QDebug>
#include <QThread>

ComSnapshot::ComSnapshot(QQuickView *view, QObject *parent)
    : QObject(parent), view(view)
{
}

double ComSnapshot::getProgress() const
{
    return progress;
}

void ComSnapshot::test()
{
    for(int i=0; i<100; ++i){
        for(int j=0; j<11900000; ++j){
            progress++;
            progressChanged();
        }
    }
    qDebug() << "progress changed...";
}

