import QtQuick 2.0
import "qrc:/bgImgs"

Rectangle {
    property string clock: "12";

    id: root
    color: "white"
    anchors.fill: parent

    Image{
        anchors.fill: root
        //fillMode: image.tile
        source: "/bgImgs/bg7.png"
        width: root.width
        height: root.height
    }

    states:[
        State {
            name: "12"
            when: root.clock == "12"
            PropertyChanges {
                target: root
                color: "white"
            }
        },
        State {
            name: "1"
            when: root.clock == "1"
            PropertyChanges {
                target: root
                color: "#D5ADAC"
            }
        },
        State {
            name: "2"
            when: root.clock == "2"
            PropertyChanges {
                target: root
                color: "#C0936D"
            }
        },
        State {
            name: "3"
            when: root.clock == "3"
            PropertyChanges {
                target: root
                color: "#C0A56D"
            }
        },
        State {
            name: "4"
            when: root.clock == "4"
            PropertyChanges {
                target: root
                color: "#D5CEAC"
            }
        },
        State {
            name: "5"
            when: root.clock == "5"
                   || root.clock == "5flat"
                   || root.clock == "5sharp"
            PropertyChanges {
                target: root
                color: "#ABBF9A"
            }
        },
        State {
            name: "6"
            when: root.clock == "6"
                  || root.clock == "6flat"
                  || root.clock == "6sharp"
            PropertyChanges {
                target: root
                color: "#8EBEE0"
            }
        },
        State {
            name: "7"
            when: root.clock == "7"
                  || root.clock == "7flat"
                  || root.clock == "7sharp"
            PropertyChanges {
                target: root
                color: "#87DDDA"
            }
        },
        State {
            name: "8"
            when: root.clock == "8"
            PropertyChanges {
                target: root
                color: "#8DC2DF"
            }
        },
        State {
            name: "9"
            when: root.clock == "9"
            PropertyChanges {
                target: root
                color: "#A996E4"
            }
        },
        State {
            name: "10"
            when: root.clock == "10"
            PropertyChanges {
                target: root
                color: "#C68FE2"
            }
        },
        State {
            name: "11"
            when: root.clock == "11"
            PropertyChanges {
                target: root
                color: "#E98FD0"
            }
        },
        State {
            name: "F596B4"
            when: root.clock == "12"
            PropertyChanges {
                target: root
                color: "#4D2E76"
            }
        }
    ]

    transitions:[
        Transition {
            ColorAnimation {
                duration: 300
                easing.type: Easing.InQuad
            }
        }

    ]
}

