import QtQuick 2.0
import "qrc:/"
import QtGraphicalEffects 1.0


Rectangle {
    id: root
    anchors.centerIn: parent

    property string key: "12"

    function calculateKey(){
        var oclock = (12-outer.currentDot);
        if(oclock == 0) return "12";
        else if(oclock == 5
                || oclock == 6
                || oclock == 7)
            return (oclock.toString() + mode.state);
        else return oclock.toString();
    }

    Item{
        id: outer
        width: 250
        height: width
        anchors.centerIn: parent
        smooth: true
        rotation: 0

        property real xOrigin: (width / 2);
        property real yOrigin: (height / 2);
        property int ptDrag: 0;
        property int ptSnap: 0;
        property double radius: width/2;
        property int currentDot: 12;


        MouseArea{
            anchors.fill: parent;

            onPressed: {
                outer.ptDrag = getMouseDeg(mouse);
            }

            onPositionChanged:  {
                var deg = getMouseDeg(mouse);
                var delta = outer.rotation - deg;
                outer.rotation = (delta + outer.ptDrag)%360;
            }

            onReleased: {
                var slice = 360 / 12;
                var half = slice/2;
                outer.ptSnap = outer.ptDrag = getSliceMid();
                outer.state = "empty";
                outer.state = "snapRotate";

                function getSliceMid(){
                    for(var i=0; i<=12; ++i)
                        if(getSliceBoundary(i)){
                            outer.currentDot = i;
                            updateStates(i);
                            return slice*i;
                        }
                    return outer.rotation;
                }

                function getSliceBoundary(pos){
                    var deg = parent.rotation % 360;
                    if(deg < 0) deg += 360;

                    var lower = (slice * pos) - (slice/2);
                    var upper = (slice * pos) + (slice/2);
                    return deg >= lower && deg < upper;
                }
            }

            function getMouseDeg(mouse){
                var point =  mapToItem (outer, mouse.x, mouse.y);
                var xPos = (point.x - outer.xOrigin);
                var yPos = (outer.yOrigin - point.y);
                var rad = (Math.atan(yPos/xPos) * 180 / Math.PI);
                if(xPos < 0) return 180 + rad;
                return rad;
            }
        }

        Rectangle{
            transformOrigin: Item.Center
            radius: outer.radius
            anchors.fill: parent
            id: inner
            antialiasing: true
            smooth: true
            border.color: "#333333"
            border.width: 2
            border.pixelAligned: true

            Rectangle{
                transformOrigin: Item.Center
                radius: outer.radius
                anchors.fill: parent
                rotation: 90
                gradient: Gradient{
                    GradientStop {
                        position: 0.0
                        color: "#e2e2e2"
                    }
                    GradientStop {
                        position: 0.50
                        color: "#dbdbdb"
                    }
                    GradientStop {
                        position: 0.51
                        color: "#d1d1d1"
                    }
                    GradientStop {
                        position: 1.0
                        color: "#E5E5E5"
                    }
                }
            }



            property double accentLetterSpacing: {
                var os = Qt.platform.os;
                if(os == "blackberry"
                   || os == "android"
                   || os == "ios")
                    return -4;
                else return 0;
            }

            CircleDot{
                id: dot12
                rotation: (360 / 12)*12
                noteText: "C"
                outerText: "12"
                insideText: "I"
                state: "selected"
            }

            CircleDot{
                id: dot1
                rotation: (360 / 12)*1
                noteText: "G"
                outerText: "1"
                insideText: "V7"
            }

            CircleDot{
                id: dot2
                rotation: (360 / 12)*2
                noteText: "D"
                outerText: "2"
                insideText: "ii"
            }

            CircleDot{
                id: dot3
                rotation: (360 / 12)*3
                noteText: "A"
                outerText: "3"
                insideText: "vi"
            }

            CircleDot{
                id: dot4
                rotation: (360 / 12)*4
                noteText: "E"
                outerText: "4"
                insideText: "iii"
            }

            CircleDot{
                id: dot5
                rotation: (360 / 12)*5
                noteText: "B"
                outerText: "5"
                insideText: "vii°"

            }


            CircleDot2Txt{
                id: dot6
                rotation: (360 / 12)*6
                txtTop: "G♭"
                txtBottom: "F♯"
                letterSpacing: inner.accentLetterSpacing
                outerText: "6"
            }

            CircleDot2Txt{
                id: dot7
                rotation: (360 / 12)*7
                txtTop: "D♭"
                txtBottom: "C♯"
                letterSpacing: inner.accentLetterSpacing
                outerText: "7"

            }

            CircleDot2Txt{
                id: dot8
                rotation: (360 / 12)*8
                txtTop: "A♭"
                txtBottom: "G♯"
                letterSpacing: inner.accentLetterSpacing
                outerText: "8"
            }

            CircleDot2Txt{
                id: dot9
                rotation: (360 / 12)*9
                txtTop: "E♭"
                txtBottom: "D♯"
                letterSpacing: inner.accentLetterSpacing
                outerText: "9"
            }

            CircleDot2Txt{
                id: dot10
                rotation: (360 / 12)*10
                txtTop: "B♭"
                txtBottom: "A♯"
                letterSpacing: inner.accentLetterSpacing
                outerText: "10"
            }

            CircleDot{
                id: dot11
                rotation: (360 / 12)*11
                noteText: "F"
                outerText: "11"
                insideText: "IV"
            }


        }

        states: State {
            name: "snapRotate"
            PropertyChanges {
                target: outer
                rotation: outer.ptSnap
            }
        }

        State{
            name: "empty"
        }

        transitions: Transition{
            RotationAnimation {
                duration: 185
                direction: RotationAnimation.Shortest
            }
        }

    }

    CircleDotAccent{
        id: mode
        visible: false
        MouseArea{
            anchors.fill: parent.circ;
            onPressed: {
                mode.state = mode.state == "sharp"
                        ? "flat" : "sharp";
                resetDotState();
                updateStates(outer.currentDot);
            }
        }
    }



    //*************************
    //** Dots and State Reset
    //*************************

    function resetDotState(){
        dot1.state = dot2.state = dot3.state = dot4.state =
                dot5.state = dot6.state = dot7.state =
                dot8.state = dot9.state = dot10.state =
                dot11.state = dot12.state = "none";
    }

    function updateStates(i){
        resetDotState();
        compl(12-i);
        selectDot(12-i);
        root.key = calculateKey();
    }

    function selectDot(pos){
        mode.visible = false;
        switch(pos){
            case 1:
                dot1.state = "selected";
                break;
            case 2:
                dot2.state = "selected";
                break;
            case 3:
                dot3.state = "selected";
                break;
            case 4:
                dot4.state = "selected";
                break;
            case 5:
                dot5.state = "selected";
                mode.visible = true;
                break;
            case 6:
                dot6.state = "selected";
                mode.visible = true;
                break;

            case 7:
                dot7.state = "selected";
                mode.visible = true;
                break;
            case 8:
                dot8.state = "selected";
                break;
            case 9:
                dot9.state = "selected";
                break;
            case 10:
                dot10.state = "selected";
                break;
            case 11:
                dot11.state = "selected";
                break;
            case 12: case 0:
                dot12.state = "selected";
                break;
        }
    }

    function compl(pos){
        complMode2(pos);

    }

    function complMode2(pos){
        if(pos === 1)
            dot11.state = "compl";
        else if (pos === 2)
            dot11.state = dot12.state =  "compl";
        else if (pos === 3)
            dot11.state = dot12.state =
            dot1.state = "compl";
        else if (pos === 4)
            dot11.state = dot12.state =
            dot1.state = dot2.state = "compl";
        else if (pos === 5 && mode.state == "sharp")
            dot11.state = dot12.state =
            dot1.state = dot2.state =
            dot3.state = "compl";
        else if(pos === 6 && mode.state == "sharp")
            dot11.state = dot12.state =
            dot1.state = dot2.state =
            dot3.state = dot4.state = "compl";
        else if(pos === 7 && mode.state == "sharp")
            dot11.state = dot12.state =
            dot1.state = dot2.state =
            dot3.state = dot4.state =
            dot5.state = "compl";

        else if(pos === 11) dot5.state = "compl";
        else if(pos === 10)
            dot5.state = dot4.state = "compl";
        else if(pos === 9)
            dot5.state = dot4.state =
            dot3.state = "compl";
        else if(pos === 8)
            dot5.state = dot4.state =
            dot3.state = dot2.state = "compl";
        else if(pos === 7 && mode.state == "flat")
            dot5.state = dot4.state =
            dot3.state = dot2.state =
            dot1.state = "compl";
        else if(pos === 6 && mode.state == "flat")
            dot5.state = dot4.state =
            dot3.state = dot2.state =
            dot1.state = dot12.state = "compl";
        else if(pos === 5 && mode.state == "flat")
            dot5.state = dot4.state =
            dot3.state = dot2.state =
            dot1.state = dot12.state =
            dot11.state = "compl";

        if(mode.state == "flat"){
            dot5.noteText = "B/Cb";
        } else
            dot5.noteText = "B";

    }


    function complMode1(pos){
        if(pos === 11) dot10.state = "compl";
        else if(pos === 10)
            dot10.state = dot9.state = "compl";
        else if(pos === 9)
            dot10.state = dot9.state =
            dot8.state = "compl";
        else if(pos === 8)
            dot10.state = dot9.state =
            dot8.state = dot7.state = "compl";
        else if(mode.state == "flat"){
            if(pos === 7) {
                dot10.state = dot9.state =
                dot8.state = dot7.state =
                dot6.state = "compl";
            } else if(pos === 6)
                dot10.state = dot9.state =
                dot8.state = dot7.state =
                dot6.state = dot5.state = "compl";
            else if(pos === 5){
                dot10.state = dot9.state =
                dot8.state = dot7.state =
                dot6.state = dot5.state =
                dot4.state = "compl";
            }
            dot5.noteText = "C♭";
            dot4.noteText = "F♭";
        }

        else if(pos === 1)
            dot6.state = "compl";
        else if (pos === 2)
            dot6.state = dot7.state =  "compl";
        else if (pos === 3)
            dot6.state = dot7.state = dot8.state = "compl";
        else if(mode.state == "sharp") {
            dot5.noteText = "B";
            dot4.noteText = "E";
            if (pos === 4)
                dot6.state = dot7.state =
                dot8.state = dot9.state = "compl";
            else if (pos === 5)
                dot6.state = dot7.state =
                dot8.state = dot9.state =
                dot10.state = "compl";
            else if(pos === 6)
                dot6.state = dot7.state =
                dot8.state = dot9.state =
                dot10.state = dot11.state = "compl";
            else if(pos === 7)
                dot6.state = dot7.state =
                dot8.state = dot9.state =
                dot10.state = dot11.state =
                dot12.state = "compl";
        }

    }


}

