import QtQuick 2.0
import QtGraphicalEffects 1.0

Item{
    transformOrigin: Item.Center
    anchors.fill: parent
    antialiasing: true
    rotation: 0
    id: root

    property string txtTop: "1";
    property string txtBottom: "1";
    property int letterSpacing: 0;
    property string outerText: "1";
    property alias circ : circ;
    property string mode: "sharp";

    Rectangle{
        id: circ
        color: "white"
        radius: width / 2
        height: 40; width: height
        antialiasing: true
        anchors{
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            margins: 10
        }

        RectangularGlow{
            id: circGlow
            glowRadius: 5
            spread: .001
            opacity: 0
            color: "orange"
            anchors.fill: circ
            cornerRadius: circ.radius + glowRadius
        }


        Text{
            text: txtTop
            font.pointSize: 10
            font.letterSpacing: letterSpacing
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: -8
            }
        }

        Text{
            text: txtBottom
            font.pointSize: 10
            font.letterSpacing: letterSpacing;
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: 8
            }
        }

        OutsideText{
            id: txtOuter
            text: outerText
            anchors{
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.top
                verticalCenterOffset: -35
            }
        }
    }

    states: [
        State{
            name: "none"
            PropertyChanges {
                target: circ
                color: "white"
                border.width: 0
                border.color: "#000"
            }
            PropertyChanges{
                target: circGlow
                opacity:  0
                color: "orange"
            }
        },
        State {
            name: "compl"
            PropertyChanges {
                target: circ
                color: "#C98934"
                border.width: 1
                border.color: "black"
            }
        },
        State {
            name: "selected"
            PropertyChanges{
                target: circGlow
                opacity: .5
            }
            PropertyChanges {
                target: circ
                border.color: "black"
                border.width: 2
            }
        }
    ]

    transitions:[
        Transition {
            to: "selected"
            NumberAnimation {
                target: circGlow
                property: "opacity"
                duration: 285
                easing.type: Easing.InBack
            }
        },
        Transition {
            to: "none"
            NumberAnimation {
                target: circGlow
                property: "opacity"
                duration: 800
                easing.type: Easing.OutBack
            }
        },

        Transition {
            to: "compl"
            ColorAnimation{
                target: circ
                property: "color"
                duration: 800
                easing.type: Easing.InBack
            }

            NumberAnimation {
                target: circ
                property: "border.width"
                duration: 800
                easing.type: Easing.InBack
            }
        },
        Transition {
            to: "none"
            ColorAnimation {
                target: circ
                property: "color"
                duration: 800
                easing.type: Easing.OutBack
            }

            NumberAnimation {
                target: circ
                property: "border.width"
                duration: 800
                easing.type: Easing.OutBack
            }
        }
    ]
}
