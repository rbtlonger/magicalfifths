import QtQuick 2.0

Canvas{
    canvasWindow: Qt.rect(0, 0, 2000, 2000)
    onImageLoaded : requestPaint();

    onPaint: {
        var ctx = getContext('2d');

        ctx.beginPath();
        ctx.moveTo(18,23);
        ctx.lineTo(18,100);
        ctx.lineTo(0,109);
        ctx.lineTo(0,158);
        ctx.lineTo(18,149);
        ctx.lineTo(18,226);
        ctx.lineTo(0,235);
        ctx.lineTo(0,284);
        ctx.lineTo(18,276);
        ctx.lineTo(18,345);
        ctx.lineTo(27,345);
        ctx.lineTo(27,271);
        ctx.lineTo(77,247);
        ctx.lineTo(77,319);
        ctx.lineTo(86,319);
        ctx.lineTo(86,244);
        ctx.lineTo(107,234);
        ctx.lineTo(107,186);
        ctx.lineTo(86,194);
        ctx.lineTo(86,117);
        ctx.lineTo(107,108);
        ctx.lineTo(107,58);
        ctx.lineTo(86,67);
        ctx.lineTo(86,0);
        ctx.lineTo(77,0);
        ctx.lineTo(77,72);
        ctx.lineTo(27,96);
        ctx.lineTo(27,23);

        ctx.fill();

        ctx.fillStyle = "white";
        ctx.beginPath();
        ctx.moveTo(27,145);

        ctx.lineTo(27,223);
        ctx.lineTo(77,198);
        ctx.lineTo(77,122);

        ctx.fill();
    }
}
