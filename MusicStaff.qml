import QtQuick 2.0

Rectangle{
    id: root
    anchors.centerIn: parent
    color: "transparent"
    height: 150
    width: 300

    Rectangle{
        anchors.centerIn: parent
        color: "transparent"
        width: 270
        height: 80
        border.width: 2
        border.color: "black"

        Rectangle{
            y: 20
            height: 2
            width: parent.width
            color: "black"
            id: second
        }

        Rectangle{
            y: 40
            height: 2
            width: parent.width
            color: "black"
            id: third
        }

        Rectangle{
            y: 60
            height: 2
            width: parent.width
            color: "black"
            id: fourth
        }


    }

}


