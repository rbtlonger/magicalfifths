#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "comsnapshot.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QQmlContext *ctx = engine.rootContext();
    QQuickView* view = qobject_cast<QQuickView*>(engine.children().at(0));
    ComSnapshot snap(view);
    ctx->setContextProperty("snap", &snap);

    return app.exec();
}
