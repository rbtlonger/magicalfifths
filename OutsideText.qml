import QtQuick 2.0

Item {
    id:root

    property string text: "1";
    property int fontSize: 9;
    property string fontColor: "black";
    property int letterSpacing: 0;

    Text{
        width: parent.width
        text: root.text
        font.pointSize: fontSize
        color: fontColor
        font.letterSpacing: letterSpacing
        horizontalAlignment: Text.AlignHCenter
    }
}

