import QtQuick 2.0
import "qrc:/staff"

Rectangle {

    SharpAccent{
        scale: .35
        x: -90; y:-85
    }

    SharpAccent{
        scale: .35
        x:-65; y:-55
    }

    SharpAccent{
        scale: .35
        x:-40; y:-95
    }
}
